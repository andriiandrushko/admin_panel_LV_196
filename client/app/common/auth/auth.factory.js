let AuthService = () => {

   let isAuth = !!localStorage.length;
   let auth = () => {
     return isAuth;
   };
   let loc = ($state) => {
     return $state.go('login');
   };
   return { auth, loc }
};

export default AuthService;