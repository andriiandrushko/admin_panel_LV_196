import angular from 'angular';
import uiRouter from 'angular-ui-router';
import navbarComponent from './navbar.component';
import './navbar.css';

const navbarModule = angular.module('navbar', [
  uiRouter
])

.component('navbarComponent', navbarComponent)
.name

export default navbarModule;

