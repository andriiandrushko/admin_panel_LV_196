class NavbarController {
  constructor($state){
    'ngInject';
    this.state = $state;
}
  $onInit(){
    this.links= [
      {name: 'Home',      link: "home", id:1},
      {name: 'Search',    link: "search", id:2},
      {name: 'User List', link: 'userlist',id:3},
      {name: 'Statistic', link: "statistic", id:4}
    ];
  }
  profile(){
    console.log(localStorage.profile.JSON())
    return this.store.get('profile');
  }
  logout(){

    this.state.go('login');
    localStorage.clear();
  }

  access(){
    return localStorage.access;
  }
};

export default NavbarController;
