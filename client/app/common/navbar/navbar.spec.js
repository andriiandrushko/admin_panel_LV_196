// import navbarModule from './navbar'
// describe('Navigation Pannel ', () => {
//   let $rootScope, $componentController, $compile, $provide;
//   let controller, template, scope,template1;
//   let linksNav;
//   beforeEach(angular.mock.module(navbarModule));
//   beforeEach(angular.mock.inject(($injector) => {
//     $rootScope = $injector.get('$rootScope');
//     $componentController = $injector.get('$componentController');
//     $compile = $injector.get('$compile');
//     controller = $componentController('navbarComponent', 
//       { $scope: $rootScope.$new()});
//     scope = $rootScope.$new();
//     template = $compile('<navbar-component></navbar-component>')(scope);
//     scope.$apply();
//    })) 
//     it('Test of conection with controller', ()=>{
//       expect(controller).toBeDefined();
//     });
//     it('Check array of Links in NavBar', () => {
//      linksNav = [
//       {name: 'Home',      link: "home", id:1},
//       {name: 'Search',    link: "search", id:2},
//       {name: 'User List', link: 'userlist',id:3},
//       {name: 'Statistic', link: "statistic", id:4}
//      ];
//       controller.$onInit();
//       expect(controller.links).toEqual(linksNav);
//      });
//     it('Check access func in Navbar', () => {
//        spyOn(controller,'access')
//       controller.access()
//        expect(controller.access).toHaveBeenCalled();
//      });
//     it('Check logout func in Navbar', () => {
//        spyOn(controller,'logout')
//       controller.logout()
//        expect(controller.logout).toHaveBeenCalled();
//     });
//     it('Check logout button in template', () => {
//        expect((template.find('button').html())).toEqual('logout');
//     });
//     it('Check number of navigation buttons', () => {
//       let listItem = template.find('li').length;
//       expect(listItem).toEqual(4);
//   });
// });
