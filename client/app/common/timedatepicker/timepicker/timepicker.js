import angular from 'angular';
import uiRouter from 'angular-ui-router';
import timepickerComponent from './timepicker.component';

const timepickerModule = angular
  .module('timepicker', [uiRouter])
  .component('timepicker', timepickerComponent)
  .name;

export default timepickerModule;