import template from './timedatepicker.html';
import controller from './timedatepicker.controller';
import './timedatepicker.css';

const timedatepickerComponent = {
  bindings: {
    date: '<',
    time: '<',
    onUpdate: '&'
  },
  template,
  controller
};

export default timedatepickerComponent;