import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Datepicker from './datepicker/datepicker';
import Timepicker from './timepicker/timepicker';
import timedatepickerComponent from './timedatepicker.component';

const timedatepickerModule = angular
  .module('timedatepicker', [uiRouter,Timepicker,Datepicker])
  .component('timedatepicker', timedatepickerComponent)
  .name;

export default timedatepickerModule;