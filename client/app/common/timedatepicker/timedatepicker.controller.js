class TimepickerController {
  constructor(){
    this.updateDate = function (event) {
      this.date = event.date;
      this.onUpdate({
        $event: {
          date: this.date,
          time: this.time
        }
      });
    };
    this.updateTime = function (event) {
      this.time = event.time;
      this.onUpdate({
        $event: {
          date: this.date,
          time: this.time
        }
      });
    };
  }
}
export default TimepickerController;


