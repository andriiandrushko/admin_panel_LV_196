class DatepickerController {
  constructor(){
    this.popupOpened = false
  }
  saveDate(){
    this.onUpdate({
      $event: {
        date: this.date
      }
    });
  }
  open(){
    this.popupOpened = true;
  }
}

export default DatepickerController;
