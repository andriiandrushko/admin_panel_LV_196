export default class authorizationService {
  'use strict';
  constructor() {
  }
  setData(data) {
        localStorage.setItem('userName',data.userName);
        localStorage.setItem('userEmail',data.userEmail);
        localStorage.setItem('userRole',data.userRole);
        localStorage.setItem('userDetails',data.userDetails);
        localStorage.setItem('userPassword',data.userPassword);
  }
  setPaswd(data) {
      localStorage.setItem('userPassword',data.userPassword);
  }
}
