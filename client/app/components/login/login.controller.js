class LoginController {
  constructor(auth, $state, store) {
    "ngInject";
    this.name = 'login';
    auth.signin({}, 
      (profile, token) => {
        store.set('profile', profile);
        store.set('token', token);
        store.set('access', true);
        $state.go('home');
      }
    );
  }
}

export default LoginController;