import template from './login.html';
import controller from './login.controller';

let loginComponent = {
  template,
  controller
};

export default loginComponent;
