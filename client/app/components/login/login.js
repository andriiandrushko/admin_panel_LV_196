import angular from 'angular';
import uiRouter from 'angular-ui-router';
import loginComponent from './login.component';

let loginModule = angular
  .module('login', [uiRouter])
  .config(function($stateProvider){
    "ngInject";
    $stateProvider
      .state('login', {
        url: '/login',
        component: 'login'
      });
  })
  .component('login', loginComponent)


export default loginModule.name;