import firebase from 'firebase';

class SearchController {
  constructor($firebaseObject,$firebaseArray,$state) {
    const rootRef = firebase.database().ref();
    this.array = $firebaseArray(rootRef);
    this.largeDB = $firebaseArray(rootRef.child('/largeDB'));
    this.activityDB = $firebaseArray(rootRef.child('/activityDB'));

    this.largeDB.$loaded().then((data) => {
      this.fullDB = data;
    });
    this.activityDB.$loaded().then((data) => {
    this.urlDB = data;
    });
    this.state = $state;
  };
  updateUser(event) {
    this.users = event.users;
  };
  updateSite(event) {
    this.sites = event.sites;
  };
  access(){
    return localStorage.access
  };
  $onInit(){
    this.dateFrom = new Date();
    this.dateTo = new Date();
    this.timeFrom = new Date();
    this.timeTo = new Date();
  };
  updateTimeFrom(event){
    this.timeFrom = event.time;
    this.dateFrom = event.date;
    this.dateFrom = fullDate(this.dateFrom, this.timeFrom);
  };
  updateTimeTo(event){
    this.timeTo = event.time;
    this.dateTo = event.date;
    this.dateTo = fullDate(this.dateTo, this.timeTo);
  };
  validPeriod(){
    return (this.dateTo < this.dateFrom);
  }
  sendFilterData(){
        this.state.go('searchResult', {
        timefrom: this.dateFrom,
        timeto: this.dateTo, 
        url: this.sites,
        name: this.users,
        fullDB: this.fullDB,
        urlDB: this.urlDB},
        );
 }
}

SearchController["$inject"]= ['$firebaseObject','$firebaseArray','$state'];
export default SearchController;

const getHM = (date) => {
  let time = [date.getHours(), date.getMinutes()];
  return time;
}

const fullDate = (date, time) => {
  let tm = getHM(time);
    date.setHours(tm[0],tm[1]);
    return date;
}