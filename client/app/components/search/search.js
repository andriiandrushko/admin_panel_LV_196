import angular from 'angular';
import uiRouter from 'angular-ui-router';
import searchComponent from './search.component';
import SearchGrid from './searchresult/search.grid';

const searchModule = angular.module('search', [
   uiRouter,
   SearchGrid
])
 .config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('search', {
      url: '/search',
      component: 'search',
      params: {
        timefrom: '',
        timeto: '',
        name: '',
        url: '',
        fullDB: []
      }
    });
})

.component('search', searchComponent)

.name;

export default searchModule;


