import angular from 'angular';
import uiRouter from 'angular-ui-router';
import SearchGridComponent from './search.grid.component';
import firebase from 'firebase';
import angularUiBootstrap from 'angular-ui-bootstrap';
import searchResultTable from './search.result.grid.config';
import SearchGridService from './search.grid.service'
import 'angularfire';

const searchGridModule = angular.module('searchResultGrid', [
  'ui.grid',
  'ui.grid.infiniteScroll',
  'ui.grid.resizeColumns',
  'ui.grid.pinning',
  'firebase',
  angularUiBootstrap,
  uiRouter
])
.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('searchResult', {
      url: '/searchResult',
      component: 'searchResult',
      params: {
       timefrom: '',
       timeto: '',
       name: '',
       url: '',
       fullDB: [],
       urlDB: []
      }
    });
})
.service('SearchGridService', SearchGridService)
.constant('SEARCHRESULTTABLE', {data: searchResultTable})
.component('searchResult', SearchGridComponent)
.name;

export default searchGridModule;
