const searchResultTable =[
  { name : "name", field: "name",cellClass: "text-center", width: 250},
  { name: "quantity_visits_domain#1", 
    displayName:"#", 
    cellClass: "text-center",
    field: "visits[0].conections.length", 
    width: 80, 
    cellTooltip: (row, col) => {
      return '  How many times '+row.entity.name+ " visit this site";},
     },
  { name: "domain#1",
    displayName:"Domain", 
    field: "visits[0].url.toString()",
    width: 250 ,
    cellTooltip: (row, col) => {
      return row.entity.name+'  walking here' ;}},
  { name: "quantity_visits_domain#2", 
    displayName:"#", 
    field: "visits[1].conections.length", 
    width: 80, 
    cellClass: "text-center",
    cellTooltip: (row, col) => {
      return '  How many times '+row.entity.name+ " visit this site" ;}},
  { name: "domain#2",
    displayName:"Domain", 
    field: "visits[1].url.toString()",
    width: 250 ,
    cellTooltip: (row, col) => {
      return row.entity.name+'  walking here';}},
  { name: "quantity_visits_domain#3",
    displayName:"#",
    field: "visits[2].conections.length",
    cellClass: "text-center",
    width: 80,
    cellClass: "text-center", 
    cellTooltip: (row, col) => {
      return '  How many times '+row.entity.name+ " visit this site";}},
   { name: "domain#3",
     displayName:"Domain", 
     field: "visits[2].url.toString()", 
     width: 250 ,
     cellClass: "text-center",
     cellTooltip: (row, col) => {
       return row.entity.name+'  walking here';}},
   { name: "quantity_visits_domain#4",
     displayName:"#", field: "visits[3].conections.length", 
     cellClass: "text-center",
     width: 80,
     cellClass: "text-center", 
     cellTooltip: (row, col) => {  
       return '  How many times '+row.entity.name+ " visit this site";}},
   { name: "domain#4",
     displayName:"Domain", 
     field: "visits[3].url.toString()",
     width: 250 ,
     cellTooltip: (row, col) => {
       return row.entity.name+'  walking here';}},
   { name: "quantity_visits_domain#5",
     displayName:"#", field: "visits[4].conections.length", 
     cellClass: "text-center",
     width: 80, 
     cellTooltip: (row, col) => {  
       return '  How many times '+row.entity.name+ " visit this site";}},
   { name: "domain#5",
     displayName:"Domain", 
     field: "visits[4].url.toString()",
     width: 250 ,
     cellClass: "text-center",
     cellTooltip: (row, col) => {
       return row.entity.name+'  walking here';}
     }
  ];

export default searchResultTable;