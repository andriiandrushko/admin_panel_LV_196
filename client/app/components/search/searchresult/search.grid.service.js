function SearchGridService ($stateParams){
    this.getUsers = $stateParams.name.selectedItem;
    this.getUrls = $stateParams.url.selectedItem;
    this.getTimeFrom = $stateParams.timefrom;
    this.getTimeTo = $stateParams.timeto;
    this.fullDB = $stateParams.fullDB;
    this.urlDB = $stateParams.urlDB;

  const chooseUsersToFilter=()=>{
    return (typeof this.getUsers =='object')? this.getUsers :  this.fullDB;
}
  const chooseUrlsToFilter=()=>{
    return (typeof this.getUrls== 'object')? this.getUrls : this.urlDB;
}
  const retypeSelectedUrlsArray=(array)=>{{
    return array.map(elem => elem = elem.url);
}}
  const selectedUsers= chooseUsersToFilter();
  const selectedUrls = chooseUrlsToFilter();
  const selectedTimeFrom = this.getTimeFrom.getTime();
  const selectedTimeTo = this.getTimeTo.getTime();
 this.urlArray = retypeSelectedUrlsArray(selectedUrls);

  const clearOneUserByURL =(User)=>{
    let selectedUrl = this.urlArray;
    let userFile = User;
    let array = userFile.visits;
    let filteredArray =array.filter((item)=>(item.url === selectedUrl[0] || item.url === selectedUrl[1] || item.url === selectedUrl[2] || item.url === selectedUrl[3]|| item.url === selectedUrl[4]|| item.url === selectedUrl[5]))
      return {name : userFile.name, visits : filteredArray}
}; 

  const clearUserArrayByUrl=(array)=>{
	  return array.map(clearOneUserByURL);
};
  const clearByTimeOneUrl = (array)=>{
    let start= selectedTimeFrom;
    let finish= selectedTimeTo;
    let name = array.url;
    let conections= array.conections;
    let filtered = conections.filter((elem) => (elem.date>start && elem.date<finish));
    let clearedItem = {
  	  url : name,
  	  conections : filtered
  }
  return clearedItem;
};
  const clearByTimeOneUser = (user) =>{
    let userfile= user;
    let name = userfile.name
    let userVisits= userfile.visits;
    let clear = userVisits.map(clearByTimeOneUrl);
    let cleanUser = {
      name : name,
      visits : clear
  }
  return cleanUser;
};
  const clearUserArrayByTime =(array)=>{
    return array.map(clearByTimeOneUser);
}

  const clearUserArrayByDate=(array)=>{
    return array.map(clearByTimeOneUser);
}
this.filteredUsersArrayByUrls = clearUserArrayByUrl(selectedUsers);
this.filteredArrayByDate = clearUserArrayByDate(this.filteredUsersArrayByUrls);

}
SearchGridService['$inject'] = ['$stateParams']
export default SearchGridService;

