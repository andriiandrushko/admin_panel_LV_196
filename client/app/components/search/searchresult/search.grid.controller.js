import firebase from 'firebase';
import angular from 'angular';
import 'angular-ui-bootstrap';

class SearchGridController {
  constructor($stateParams, SEARCHRESULTTABLE,SearchGridService) {
    const service = SearchGridService;
    this.getTimeFrom= service.getTimeFrom.toDateString();
    this.getTimeTo=  service.getTimeTo.toDateString();
    this.urlArray= service.urlArray;
    console.log(this.urlArray);
  this.serchResultTable = {
	  infiniteScrollRowsFromEnd: 21,
    infiniteScrollUp: true,
    infiniteScrollDown: true,
	  enableSorting: true,
	  enableFiltering: true,
	  enableColumnResizing: true,
	  enablePinning: true,
	  data: service.filteredArrayByDate,
	  columnDefs:SEARCHRESULTTABLE.data}
}}

SearchGridController['$inject'] = ['$stateParams', 'SEARCHRESULTTABLE','SearchGridService'];
export default SearchGridController;




