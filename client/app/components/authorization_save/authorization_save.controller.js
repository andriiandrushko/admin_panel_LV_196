import firebase from 'firebase';
class SaveController {
  constructor($firebaseObject, $firebaseArray, ResultsFinder) {
    const rootRef = firebase.database().ref();
    const ur = rootRef.child('/usersDBI');
    this.arr = $firebaseArray(ur);
    this.service = ResultsFinder.getResults();
    this.getItems = ResultsFinder.getItem();
  }
  $onInit() {
    this.showData = this.service;
    this.submitted = true;
  }
    submit() {
        localStorage.access = true;
          this.arr.$add(this.getItems);
    }
}
SaveController['$inject'] = ['$firebaseObject', '$firebaseArray', 'ResultsFinder'];
export default SaveController;
