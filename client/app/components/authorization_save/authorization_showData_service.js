 export default class ResultsFinder {
   'use strict';
    constructor() {
    }
    getResults() {
      const showData = [{
        name: localStorage.getItem('userName'),
        email: localStorage.getItem('userEmail'),
        details: localStorage.getItem('userDetails'),
        role: localStorage.getItem('userRole'),
        image: localStorage.getItem('image')
      }];
      return showData;
    }
   getItem() {
     const dataFromLoc = {
       name: localStorage.getItem('userName'),
       password: localStorage.getItem('userPassword'),
       email: localStorage.getItem('userEmail'),
       details: localStorage.getItem('userDetails'),
       role: localStorage.getItem('userRole'),
       image: localStorage.getItem('image')
     };
     return dataFromLoc;
   }
 }
