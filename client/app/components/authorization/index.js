import angular from 'angular';
import uiRouter from 'angular-ui-router';
import authorizationComponent from './authorization.component';

const authorizationModule = angular.module('authorization', [
  uiRouter
])

.config(($stateProvider) => {
  "ngInject";

  $stateProvider
    .state('authorization', {
      url: '/authorization',
      component: 'authorization'
    })

    .state('authorization.password', {
      url: '/password',
      template: '<password></password>'
    })

    .state('authorization.file', {
      url: '/file',
      template: '<file></file>'
    })

    .state('authorization.save', {
      url: '/save',
      template: '<save></save>'
    })
})
.component('authorization', authorizationComponent)

.name;

export default authorizationModule;
