import template from './ita.html';

const itaComponent = {
  bindings: {
    config:'<'
    },
  template
};

export default itaComponent;
