import angular from 'angular';
import uiRouter from 'angular-ui-router';
import itaComponent from './ita.component';
import amChartsDirective from './amChartsDirective';
import './itaChart.styl';

const itaModule = angular.module('ita', [
  'amChartsDirective'
])

.component('itaChart', itaComponent)
  
.name;

export default itaModule;
