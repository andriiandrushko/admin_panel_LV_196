export default class amchRender {
  'use strict';
  constructor() {
  }
  getResults(amch) {
    const db = [];
    amch.$loaded().then((data) =>{
      const amchartsBase = data;
      amchartsBase[0].map(function(item){
        const amch = {
          country: item.country,
          visits: item.visits,
        };
        db.push(amch)
      });
    });
    const progresschart = {
      amChartOptions: {
        data: db,
        "type": "serial",
        "theme": "light",
        "valueAxes": [ {
          "gridColor": "#0000FF",
          "gridAlpha": 0.2,
          "dashLength": 0} ],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [ {
          "balloonText": "[[category]]: <b>[[value]]</b>",
          "fillAlphas": 0.8,
          "lineAlpha": 0.2,
          "type": "column",
          "valueField": "visits" } ]}};
    return progresschart;
  }
    getResultTwo(amch){
      const db = [];
      amch.$loaded().then((data) =>{
        const amchartsBase = data;
        amchartsBase[1].map(function(item){
          const amch = {
            country: item.country,
            litres: item.litres,
          };
          db.push(amch)
        });
      });
      const piechart = {
        amChartOptions: {
          data: db,
          type: "pie",
          theme: 'light',
          valueField: "litres",
          titleField: "country",
          balloon:{
            fixedPosition:true
          }}};
      return piechart;
    }
    getResultThree(amch){
      const db = [];
      amch.$loaded().then((data) =>{
        const amchartsBase = data;
        amchartsBase[2].map(function(item){
          const amch = {
            country: item.country,
            visits: item.visits,
            color: item.color
          };
          db.push(amch)
        });
      });
      const profitschart = {
        id: 'myThirdChart',
        height: '300px',
        background: '#CD0D74',
        width: '600px',
        amChartOptions: {
          data:db,
          "type": "serial",
          "theme": "light",
          "valueAxes": [{
            "axisAlpha": 0,
            "position": "left",
            "title": "Visitors from country"
          }],
          "gridAboveGraphs": true,
          "startDuration": 1,
          "graphs": [{
            "balloonText": "<b>[[category]]: [[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "type": "line",
            "valueField": "visits"
          }]
        }
      };
    return profitschart;
  }
}

