import template from './statistic.html';
import controller from './statistic.controller';
import './statistic.styl';

const statisticComponent = {
  bindings: {},
  template,
  controller
};

export default statisticComponent;
