class PasswordController {
  constructor(authorizationService) {
    "ngInject";
      this.sendData = authorizationService;
  }
  $onInit(){
    this.userData = {
      userPassword : ''
    };
  }
    submit =()=> {
    if (this.form.$valid) {
      this.sendData.setPaswd(this.userData);
    }
  };
}
PasswordController['$inject'] = ['authorizationService'];
export default PasswordController;
