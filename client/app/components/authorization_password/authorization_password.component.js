import template from './authorization_password.html';
import controller from './authorization_password.controller';

const passwordComponent = {
  template,
  controller
};

export default passwordComponent;
