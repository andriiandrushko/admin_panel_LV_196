import angular from 'angular';
import uiRouter from 'angular-ui-router';
import passwordComponent from './authorization_password.component';

const passwordModule = angular.module('password', [
  uiRouter
])

.config(($stateProvider) => {
  "ngInject";

  $stateProvider
    .state('password', {
      url: '/authorization-password',
      component: 'password'
    })    
})

.component('password', passwordComponent)

.name;

export default passwordModule;