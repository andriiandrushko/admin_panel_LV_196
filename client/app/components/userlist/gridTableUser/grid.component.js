import template from './grid.html';
import controller from './grid.controller';
import './grid.styl';

const gridComponent = {
  bindings: {
    rows: '<'
  },
  template,
  controller
};

export default gridComponent;