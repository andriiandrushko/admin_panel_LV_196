// import GridModule from './grid';
// import GridController from './grid.controller';
// import GridComponent from './grid.component';
// import GridTemplate from './grid.html';

// describe('GridTable', () => {
//   const $rootScope, makeController;
//   beforeEach(window.module(GridModule));
//   beforeEach(inject((_$rootScope_) => {
//     $rootScope = _$rootScope_;
//     makeController = () => {
//       return new GridController();
//     };
//   }));
  
//   describe('Controller', () => {
//     it('has a usersTable property for creating UI-grid table ', () => { 
//       const controller = makeController();
//       expect(controller).to.have.property('usersTable');
//     });
//   });

//   describe('Component', () => {
//     const component = GridComponent;
//     it('includes the intended template',() => {
//       expect(component.template).to.equal(GridTemplate);
//     });
//     it('invokes the right controller', () => {
//       expect(component.controller).to.equal(GridController);
//     });
//   });
  
//   describe('grid on the page, no default sort', () => {
//     it('header values should be as expected', () => {
//       GridTable.expectHeaderColumns( [ 'name', 'role', 'email', 'details', 'userOption' ] );
//   });
//     it('sort ascending by clicking menu', () => {
//       GridTable.clickColumnMenuSortAsc( 0 )
//         .then(function () { GridTable.expectCellValueMatch( 0, 0, 'Anonimus Guest' );     
//       });
//     });
//   });
// });
