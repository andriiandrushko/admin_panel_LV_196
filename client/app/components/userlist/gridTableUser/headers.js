const headers = [
  { field: "name"},
  { field: "image" ,enableSorting: false,enableFiltering: false,cellTemplate:"<img class='img' width=\"50px\" ng-src=\"{{grid.getCellValue(row, col)}}\" lazy-src>"},
  { field: "role",  cellClass:"rolefor"},
  { field: "email", cellClass:"smaller", cellTooltip: (row, col) => {return 'Email of : '+ row.entity.name;}},
  { field: "details", cellTooltip: (row, col) => {return 'This is additional info about: '+ row.entity.name;}},
  { name: 'userOption',
    enableSorting: false,
    enableFiltering: false,
    displayName: 'User Option',
    cellTemplate: `<button class="btn btn-primary" ng-click="grid.appScope.$ctrl.edit(row)">Edit</button>
<button class="btn btn-danger" ng-click="grid.appScope.$ctrl.deleteUser(row.entity)">Delete</button>`}];
export default headers;

