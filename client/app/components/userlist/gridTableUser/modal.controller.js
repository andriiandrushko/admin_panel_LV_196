class EditCtrl {
  constructor (row, users, $uibModalInstance) {
    this.row = row;
    this.ed = () => {
      row.entity.name = (this.name || row.entity.name);
      row.entity.email = (this.email || row.entity.email);
      row.entity.role = (this.role || row.entity.role);
      row.entity.details = (this.details || row.entity.details);
      $uibModalInstance.close(users.$save(row.entity));
    };
    this.del = () => {
      $uibModalInstance.close(users.$remove(row));
    };
    this.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }
}

EditCtrl['$inject'] = ['row', 'users', '$uibModalInstance'];
export default EditCtrl;
