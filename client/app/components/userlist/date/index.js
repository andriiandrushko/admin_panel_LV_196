import angular from 'angular';
import dateComponent from './date.component';

const dateModule = angular.module('date', [])

.component('date', dateComponent)

.name;

export default dateModule;