import template from './additional_settings.html';
import controller from './additional_settings.controller';
import './additional_settings.styl';

const settingsComponent = {
  bindings: {
    save: '&'
  },
  template,
  controller
};

export default settingsComponent;