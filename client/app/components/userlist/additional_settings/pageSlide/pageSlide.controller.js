class SlideController {
  constructor($filter) {
    this.filter = $filter;
  }

  $onInit() {
    const DEFAULT_FORMAT = 'MMMM d, y';
    const DEFAULT_ROWS_NUMBER = 10;
    this.savingDateFormat = localStorage.getItem("dateFormat") || DEFAULT_FORMAT;
    this.savingRowsNumber = localStorage.getItem("numberOfTableRows") || DEFAULT_ROWS_NUMBER;
    this.format = this.savingDateFormat;
    this.rowsNumber = Number(this.savingRowsNumber);
    this.date = this.filter('date')(new Date(), this.format);
    this.longDate = this.filter('date')(new Date(), 'MMMM d, y');
    this.shortDate = this.filter('date')(new Date(), 'M/d/yy');
    this.mediumDate = this.filter('date')(new Date(), 'MMM d, y');
    this.fullDate = this.filter('date')(new Date(), 'EEEE, MMMM d, y');

    this.changes = {
      'datepicker': this.format,
      'rowsNumber': this.rowsNumber
    };
  }

  saveChanges() {
    this.format = this.changes.datepicker;
    this.rowsNumber = this.changes.rowsNumber;

    localStorage.setItem("dateFormat", this.format);
    localStorage.setItem("numberOfTableRows", this.rowsNumber);

    this.savingDateFormat = localStorage.getItem("dateFormat");
    this.date = this.filter('date')(new Date(), this.savingDateFormat);

    this.savedSettings({
      newSettings : {
        dateFormat: this.format,
        rowsNumber: this.rowsNumber
      }
    });
  }
}

SlideController['$inject'] = ['$filter'];

export default SlideController;