import template from './pageSlide.html';
import controller from './pageSlide.controller';
import './pageSlide.styl';

const slideComponent = {
  bindings: {
    toggle: '&',
    savedSettings: '&'
  },
  template,
  controller
};

export default slideComponent;