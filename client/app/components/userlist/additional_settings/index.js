import angular from 'angular';
import settingsComponent from './additional_settings.component';
import pageSlide from './pageSlide/index';

const settingsModule = angular.module('settings', [
  pageSlide
])

.component('settings', settingsComponent)

.name;

export default settingsModule;